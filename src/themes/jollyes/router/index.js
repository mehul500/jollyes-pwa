const Home = () => import(/* webpackChunkName: "vsf-home" */ 'theme/pages/Home.vue')
const PageNotFound = () => import(/* webpackChunkName: "vsf-not-found" */ 'theme/pages/PageNotFound.vue')
const ErrorPage = () => import(/* webpackChunkName: "vsf-error" */ 'theme/pages/Error.vue')
const Product = () => import(/* webpackChunkName: "vsf-product" */ 'theme/pages/Product.vue')
const Category = () => import(/* webpackChunkName: "vsf-category" */ 'theme/pages/Category.vue')
const CmsPage = () => import(/* webpackChunkName: "vsf-cms" */ 'theme/pages/CmsPage.vue')
const Checkout = () => import(/* webpackChunkName: "vsf-checkout" */ 'theme/pages/Checkout.vue')
const MyAccount = () => import(/* webpackChunkName: "vsf-my-account" */ 'theme/pages/MyAccount.vue')
const ResetPassword = () => import(/* webpackChunkName: "vsf-reset-password" */ 'theme/pages/ResetPassword.vue');
const Store = () => import(/* webpackChunkName: "vsf-store" */ 'theme/pages/Store.vue')
const StoreLocator = () => import(/* webpackChunkName: "vsf-locator" */ 'theme/pages/StoreLocator.vue')
const Faq = () => import(/* webpackChunkName: "vsf-faq" */ 'theme/pages/Faq.vue')
const FaqDetail = () => import(/* webpackChunkName: "vsf-faq" */ 'theme/pages/FaqDetail.vue')
const TermsAndConditions = () => import(/* webpackChunkName: "vsf-termsandconditions" */ 'theme/pages/TermsAndConditions.vue')
const since1971 = () => import(/* webpackChunkName: "vsf-since1971" */ 'theme/pages/since-1971.vue')
const SearchResult = () => import(/* webpackChunkName: "vsf-search-result" */ 'theme/pages/SearchResult.vue')
const Brand = () => import(/* webpackChunkName: "vsf-search-result" */ 'theme/pages/Brand.vue')
const ThankYou = () => import(/* webpackChunkName: "vsf-thank-you" */ 'theme/pages/ThankYou.vue')

let routes = [
  { name: 'home', path: '/', component: Home, alias: '/pwa.html' },
  { name: 'checkout', path: '/checkout', component: Checkout },
  { name: 'my-account', path: '/my-account', component: MyAccount },
  { name: 'my-shipping-details', path: '/my-account/shipping-details', component: MyAccount, props: { activeBlock: 'MyShippingDetails' } },
  { name: 'my-newsletter', path: '/my-account/newsletter', component: MyAccount, props: { activeBlock: 'MyNewsletter' } },
  { name: 'my-orders', path: '/my-account/orders', component: MyAccount, props: { activeBlock: 'MyOrders' } },
  { name: 'my-order', path: '/my-account/orders/:orderId', component: MyAccount, props: { activeBlock: 'MyOrder' } },
  { name: 'my-recently-viewed', path: '/my-account/recently-viewed', component: MyAccount, props: { activeBlock: 'MyRecentlyViewed' } },
  { name: 'error', path: '/error', component: ErrorPage },
  { name: 'virtual-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'bundle-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'simple-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'downloadable-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'grouped-product', path: '/p/:parentSku/:slug', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'configurable-product', path: '/p/:parentSku/:slug/:childSku', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'product', path: '/p/:parentSku/:slug/:childSku', component: Product }, // :sku param can be marked as optional with ":sku?" (https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js#L16), but it requires a lot of work to adjust the rest of the site
  { name: 'category', path: '/c/:slug', component: Category },
  { name: 'terms-and-conditions', path: '/terms-and-conditions', component: TermsAndConditions },
  { name: 'since-1971', path: '/since-1971', component: since1971 },
  { name: 'cms-page', path: '/i/:slug', component: CmsPage },
  { name: 'create-password', path: '/create-password', component: ResetPassword },
  { name: 'locator', path: '/locator', component: StoreLocator },
  { name: 'store', path: '/store/:storeId', component: Store },
  { name: 'petclub-points', path: '/my-account/petclub-points', component: MyAccount, props: { activeBlock: 'PetclubPoints' } },
  { name: 'petclub-vouchers', path: '/my-account/petclub-vouchers', component: MyAccount, props: { activeBlock: 'PetclubVouchers' } },
  { name: 'payment-methods', path: '/my-account/payment-methods', component: MyAccount, props: { activeBlock: 'PaymentMethods' } },
  { name: 'stores', path: '/my-account/stores', component: MyAccount, props: { activeBlock: 'Stores' } },
  { name: 'favourites', path: '/my-account/favourites', component: MyAccount, props: { activeBlock: 'Favourites' } },
  { name: 'faq', path: '/faq', component: Faq, props: { activeBlock: 'Faq' } },
  { name: 'faqDetail', path: '/faq/:category', component: FaqDetail, props: { activeBlock: 'FaqDetail' } },
  { name: 'page-not-found', path: '*', component: PageNotFound },
  { name: 'search-result', path: '/search-result', component: SearchResult },
  { name: 'brand', path: '/brand/:slug', component: Brand },
  { name: 'thank-you', path: '/thank-you', component: ThankYou }
]

export default routes
