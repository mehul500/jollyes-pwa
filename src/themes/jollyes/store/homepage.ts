import { prepareQuery } from '@vue-storefront/core/modules/catalog/queries/common'

export const homepageStore = {
  namespaced: true,
  state: {
    dataPuppy: [],
    dataKitten: [],
    todayDeal: [],
    bestDogData: [],
    coolingRangeProducts: []
  },
  actions: {
    async loadDataFromEs ({ commit, dispatch }, payload) {
      const { items } = await dispatch('product/findProducts', {
        query: prepareQuery({ filters: payload.filters }),
        start: 0,
        size: payload.size,
        options: { populateRequestCacheTags: true, prefetchGroupProducts: true }
      }, { root: true })

      if (payload.type === 'dataPuppy') {
        commit('SET_DATAPUPPY', items)
      } else if (payload.type === 'dataKitten') {
        commit('SET_DATAKITTEN', items)
      } else if (payload.type === 'todayDeal') {
        commit('SET_TODAYDEAL', items)
      } else if (payload.type === 'bestDogData') {
        commit('SET_BEST_DOG_DATA', items)
      } else if (payload.type === 'coolingRangeProducts') {
        commit('SET_COOLING_RANGE_PRODUCTS', items)
      }
    }
  },
  mutations: {
    SET_DATAPUPPY (state, dataPuppy) {
      state.dataPuppy = dataPuppy
    },
    SET_DATAKITTEN (state, dataKitten) {
      state.dataKitten = dataKitten
    },
    SET_TODAYDEAL (state, todayDeal) {
      state.todayDeal = todayDeal
    },
    SET_BEST_DOG_DATA (state, bestDogData) {
      state.bestDogData = bestDogData
    },
    SET_COOLING_RANGE_PRODUCTS (state, coolingRangeProducts) {
      state.coolingRangeProducts = coolingRangeProducts
    }
  },
  getters: {
    getDataPuppy (state) {
      return state.dataPuppy
    },
    getDataKitten (state) {
      return state.dataKitten
    },
    getTodayDeal (state) {
      return state.todayDeal
    },
    getBestDogData (state) {
      return state.bestDogData
    },
    getCoolingRangeProductList (state) {
      return state.coolingRangeProducts
    }
  }
}
