import config from 'config'
import { currentStoreView, adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import rootStore from '@vue-storefront/core/store'

export function getPathForStaticPage (path: string) {
  const { storeCode } = currentStoreView()
  const isStoreCodeEquals = storeCode === config.defaultStoreCode

  return isStoreCodeEquals ? `/i${path}` : path
}
export function getCategoryUrl (path: string) {
  const catMageUrl = config.aureatelabs.categoryUrl.path
  const defaultImage = config.aureatelabs.categoryUrl.default
  return path ? catMageUrl + path : defaultImage
}

export function getPromoUrl (path: string) {
  let url = config.aureatelabs.categoryUrl.imgUrl
  return (typeof path !== 'undefined') ? url + path : url
}

export function getPromoImage (path: string) {
  let url = config.aureatelabs.categoryUrl.imgPath
  const defaultImage = config.aureatelabs.categoryUrl.default
  return path ? url + path : defaultImage
}

export function getFbId () {
  return '354887379623416'
}

export function setCookie (name, value, days) {
  let expires = '';
  if (days) {
    let date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = '; expires=' + date.toUTCString();
  }
  document.cookie = name + '=' + (value || '') + expires + '; path=/';
}

export function getCookie (name) {
  let nameEQ = name + '=';
  let ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

export function eraseCookie (name) {
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

export function correctUrl (image) {
  return (image && image != null && image.replace) ? image.replace(/https:\/\/backend.jollyes.co.uk\/|https:\/\/backenddev.jollyes.co.uk\//g, '/') : '';
}
export function productImageUrl (thumbnail, pageName) {
  return thumbnail ? thumbnail + '?profile=' + pageName : '';
}
