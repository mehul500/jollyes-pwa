import dayjs from 'dayjs'
import { mapActions } from 'vuex'
import { mapState } from 'vuex'
import { getCookie, setCookie, eraseCookie } from 'theme/helpers'

export default {
  data () {
    return {
      isYourStore: ''
    }
  },
  computed: {
    ...mapState({
      currentUser: state => state.user.current
    }),
    getStoreId () {
      if (this.isYourStore === '') {
        if (getCookie('storelocator') !== null) {
          const storeJsonData = JSON.parse(getCookie('storelocator'));
          return storeJsonData.warehouseId ? storeJsonData.warehouseId : storeJsonData.store_code
        }
      }
      return this.isYourStore;
    }
  },
  methods: {
    ...mapActions({
      openLogin: 'ui/toggleLogin'
    }),
    isStoreLocalStore (storeItem) {
      let hideFlag = true;
      if (this.getStoreId === storeItem.id || parseInt(this.getStoreId) === parseInt(storeItem.warehouseId)) {
        hideFlag = false
      }
      return hideFlag
    },
    storecloseTime (storeData) {
      const datedata = dayjs().get('d')
      let closingTime = '';
      if (datedata === 0) {
        closingTime = storeData.sundayClosing
      } else if (datedata === 1) {
        closingTime = storeData.mondayClosing
      } else if (datedata === 2) {
        closingTime = storeData.tuesdayClosing
      } else if (datedata === 3) {
        closingTime = storeData.wednesdayClosing
      } else if (datedata === 4) {
        closingTime = storeData.thursdayClosing
      } else if (datedata === 5) {
        closingTime = storeData.fridayClosing
      } else if (datedata === 6) {
        closingTime = storeData.saturdayClosing
      }
      return this.timeConvert(closingTime);
    },
    timeConvert (time) {
      if (time !== null) {
        time = time.replace(/(..?)/g, '$1:').slice(0, -1)
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
          time = time.slice(1); // Remove full string match value
          time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
          time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
      }
      return null
    },
    addToLocalStore (storeData) {
      if (!this.currentUser) {
        this.openLogin();
      } else {
        this.$bus.$emit('change-store-text', storeData.name)
        const localstoredata = getCookie('storelocator');
        if (localstoredata !== null) {
          eraseCookie('storelocator');
        }
        setCookie('storelocator', JSON.stringify(storeData));
        this.isYourStore = storeData.warehouseId ? storeData.warehouseId : storeData.id
      }
    },
    getStoreTimings (storeItem) {
      return [
        { weekDay: 'Mon:', startTime: this.timeConvert(storeItem.mondayOpening) || null, endTime: this.timeConvert(storeItem.mondayClosing) || null },
        { weekDay: 'Tue:', startTime: this.timeConvert(storeItem.tuesdayOpening) || null, endTime: this.timeConvert(storeItem.tuesdayClosing) || null },
        { weekDay: 'Wed:', startTime: this.timeConvert(storeItem.wednesdayOpening) || null, endTime: this.timeConvert(storeItem.wednesdayClosing) || null },
        { weekDay: 'Thu:', startTime: this.timeConvert(storeItem.thursdayOpening) || null, endTime: this.timeConvert(storeItem.thursdayClosing) || null },
        { weekDay: 'Fri:', startTime: this.timeConvert(storeItem.fridayOpening) || null, endTime: this.timeConvert(storeItem.fridayClosing) || null },
        { weekDay: 'Sat:', startTime: this.timeConvert(storeItem.saturdayOpening) || null, endTime: this.timeConvert(storeItem.saturdayClosing) || null },
        { weekDay: 'Sun:', startTime: this.timeConvert(storeItem.sundayOpening) || null, endTime: this.timeConvert(storeItem.sundayClosing) || null }
      ].filter(item => item.startTime !== null)
    },
    getFormatedUrlForStore ({ url, width = 60, height = 60, type }) {
      return `https://media.graphcms.com/${type === 'circle' ? type + '/' : ''}auto_image/resize=height:${height},width:${width}/compress/${url}`
    }
  }
}
