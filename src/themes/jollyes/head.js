export default {
  title: 'Default Theme',
  titleTemplate: '%s',
  htmlAttrs: {
    lang: 'en'
  },
  meta: [
    { charset: 'utf-8' },
    { vmid: 'description', name: 'description', content: '' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1, minimal-ui' },
    { name: 'robots', content: 'no-index, no-follow' },
    { name: 'mobile-web-app-capable', content: 'yes' },
    { name: 'theme-color', content: '#ffffff' },
    { name: 'apple-mobile-web-app-status-bar-style', content: '#ffffff' }
  ],
  link: [
    { rel: 'icon', type: 'image/png', href: '/assets/favicon-32x32.png', sizes: '32x32' },
    { rel: 'icon', type: 'image/png', href: '/assets/favicon-16x16.png', sizes: '16x16' },
    { rel: 'apple-touch-icon', href: '/assets/apple-touch-icon.png' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_2048.png', sizes: '2048x2732' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1668.png', sizes: '1668x2224' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1536.png', sizes: '1536x2048' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1125.png', sizes: '1125x2436' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1242.png', sizes: '1242x2208' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_750.png', sizes: '750x1334' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_640.png', sizes: '640x1136' },
    { rel: 'manifest', href: '/assets/manifest.json' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/montserrat-extrabold.woff2', crossorigin: 'anonymous' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/montserrat-regular.woff2', crossorigin: 'anonymous' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/Montserrat-Medium.woff2', crossorigin: 'anonymous' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/Montserrat-SemiBold.woff2', crossorigin: 'anonymous' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/montserrat-bold.woff2', crossorigin: 'anonymous' },
    { rel: 'preconnect', as: 'script', href: 'https://scripts.sirv.com', crossorigin: '' },
    { rel: 'preconnect', as: 'script', href: 'https://jollyes.sirv.com', crossorigin: '' },
    { rel: 'dns-prefetch', as: 'script', href: 'https://scripts.sirv.com' },
    { rel: 'dns-prefetch', as: 'script', href: 'https://jollyes.sirv.com' },
    { rel: 'preload', as: 'script', href: 'https://scripts.sirv.com/sirv.nospin.js' }
  ],
  script: [
    {
      rel: 'dns-prefetch',
      src: 'https://cdn.jsdelivr.net/npm/pwacompat@2.0.6/pwacompat.min.js',
      async: true,
      integrity: 'sha384-GOaSLecPIMCJksN83HLuYf9FToOiQ2Df0+0ntv7ey8zjUHESXhthwvq9hXAZTifA',
      crossorigin: 'anonymous'
    },
    {
      rel: 'dns-prefetch',
      src: '//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js',
      async: true,
      type: 'text/javascript'
    }
  ],
  noscript: []
}
