import { StoresState } from '../types/StoresState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<StoresState, any> = {
  getStoreList: (state) => state.stores,
  storeDetail: (state) => state.stores
}
