import { ActionTree } from 'vuex';
import { StoresState } from '../types/StoresState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import config from 'config';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore';
import rootStore from '@vue-storefront/core/store';
import fetch from 'isomorphic-fetch';
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';

const alStoreEntityName = config.aureatelabs.stores;

const actions: ActionTree<StoresState, any> = {
  async storeList (context) {
    try {
      let url = rootStore.state.config.aureatelabs.soterList.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      url = processLocalizedURLAddress(url);
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.STORES_FETCH_STORE_LIST, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'stores')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'stores')()
    }
  },
  async storeDetail (context, storeDetail) {
    try {
      let url = rootStore.state.config.aureatelabs.soterDetail.endpoint
      url = processLocalizedURLAddress(url);

      await fetch(url + '/' + storeDetail.storeId, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.STORES_FETCH_STORE_DETAIL, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'stores')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'stores')()
    }
  },
  async searchStoreList (context, params: any) {
    try {
      let url = rootStore.state.config.aureatelabs.searchStoreList.endpoint
      let longitude = params.longitude
      let latitude = params.latitude
      url = url + '/' + longitude + '/' + latitude
      url = processLocalizedURLAddress(url);
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            data.result = data.result.map((a) => { a.map.distance = (a.map.distance / 1609); return a })
            var searchResult = data.result.slice(0);
            searchResult.sort((a, b) => {
              return a.map.distance - b.map.distance;
            });
            context.commit(types.STORES_FETCH_STORE_LIST, searchResult.slice(0, 3))
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'stores')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'stores')()
    }
  }
};
export default actions;
