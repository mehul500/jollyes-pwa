import { Module } from 'vuex'
import { StoresState } from '../types/StoresState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'

export const storesModuleStore: Module<StoresState, any> = {
  namespaced: true,
  state: {
    stores: []
  },
  mutations,
  actions,
  getters
}
