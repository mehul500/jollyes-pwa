import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.STORES_FETCH_STORE_LIST] (state, stores) {
    state.stores = stores || []
  },
  [types.STORES_FETCH_STORE_DETAIL] (state, storeDetail) {
    state.stores = storeDetail || []
  }
}
