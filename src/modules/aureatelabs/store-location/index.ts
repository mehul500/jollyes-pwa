import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { storesModuleStore } from './store/index';

export const StoresModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('store-location', storesModuleStore)
};
