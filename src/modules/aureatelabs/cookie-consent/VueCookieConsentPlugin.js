/**
  # The MIT License (MIT)

  Copyright (c) Dans Ma Culotte <contact@dansmaculotte.fr>

  > Permission is hereby granted, free of charge, to any person obtaining a copy
  > of this software and associated documentation files (the "Software"), to deal
  > in the Software without restriction, including without limitation the rights
  > to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  > copies of the Software, and to permit persons to whom the Software is
  > furnished to do so, subject to the following conditions:
  >
  > The above copyright notice and this permission notice shall be included in
  > all copies or substantial portions of the Software.
  >
  > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  > OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  > THE SOFTWARE.
 */

export default {
  install (Vue, options = {}) {
    const initCookieConsent = () => {
      const root = new Vue()
      root._script = document.createElement('script');
      root._script.type = 'text/javascript';
      root._script.async = true;
      root._script.crossorigin = 'anonymous';
      root._script.setAttribute('data-cookiefirst-key', '020b3911-ca74-4cc9-9f3f-10f233056294');
      root._script.setAttribute('rel', 'dns-prefetch');
      root._script.id = 'cookie-conset';
      root._script.src = 'https://consent.cookiefirst.com/banner.js';
      const first = document.getElementsByTagName('script')[0];
      first.parentNode.insertBefore(root._script, first);

      root._script.onload = (event) => {
        root.$emit('loaded', event);
      }

      Vue.prototype.$cookieconsent = root;
    }
    const initWebPushScript = () => {
      // register webpush script
      let wpScriptTag = document.createElement('script')
      wpScriptTag.setAttribute('type', 'text/javascript')
      wpScriptTag.setAttribute('async', true)
      wpScriptTag.setAttribute('src', 'https://api.pushio.com/webpush/sdk/wpIndex_min.js')
      wpScriptTag.setAttribute('id', 'rsyswpsdk')
      wpScriptTag.setAttribute('wpconfig', '{"appserviceKey":"BK28yn79e1aPulEl9vQO0pG8nymM5Rp-yM_XcjjPDHgtT9QQM50Fl_a-fS09DyfSHYuDe3o9x7bxN9WjM4ywlWs=","apiKey":"ABEhq-5Doe6UspAYpI06Pe42o","accountToken":"ABEqJCBrATubvyENvCcAMhCx0","appver":"1.0.0","apiHost":"https://api.pushio.com","lazy":true}')
      document.head.appendChild(wpScriptTag)
    }
    setTimeout(initCookieConsent, options.initTimeout);
    setTimeout(initWebPushScript, 5000);
  }
};
