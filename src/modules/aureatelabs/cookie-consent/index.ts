import Vue from 'vue';
import CookieConsent from './VueCookieConsentPlugin';
import { isServer } from '@vue-storefront/core/helpers';
import { StorefrontModule } from '@vue-storefront/core/lib/modules';

export const CookieconsentModule: StorefrontModule = async function ({ appConfig }) {
  if (isServer) return;

  const cookieconsentConfig = {
    initTimeout: (appConfig.aureatelabs.cookieConsentTimeOut * 1000) || 0
  }
  Vue.use(CookieConsent, cookieconsentConfig)
}
