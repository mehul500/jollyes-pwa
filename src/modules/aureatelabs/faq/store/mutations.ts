import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_FAQ] (state, stores) {
    state.faqDetails = stores || []
  },
  [types.FETCH_ALL_FAQ] (state, stores) {
    state.faqDetails = stores || []
  }
}
