import { FaqState } from '../types/FaqState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<FaqState, any> = {
  getFaq: (state) => state.faqDetails,
  getFaqDetail: (state) => state.faqDetails
}
