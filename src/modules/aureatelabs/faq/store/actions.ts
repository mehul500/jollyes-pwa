import { ActionTree } from 'vuex';
import { FaqState } from '../types/FaqState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import fetch from 'isomorphic-fetch'
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers'
import rootStore from '@vue-storefront/core/store'

const actions: ActionTree<FaqState, any> = {
  async faqList (context) {
    try {
      let url = rootStore.state.config.aureatelabs.faq.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      url = processLocalizedURLAddress(url);
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.FETCH_FAQ, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'faq')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'faq')()
    }
  },
  async faqDetail (context, faqDetail) {
    try {
      let url = rootStore.state.config.aureatelabs.faq.all_faq_endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      url = processLocalizedURLAddress(url);
      await fetch(url + '/' + faqDetail.category, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.FETCH_ALL_FAQ, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'faq')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'faq')()
    }
  }
};
export default actions;
