import { Module } from 'vuex'
import { FaqState } from '../types/FaqState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'

export const faqModuleStore: Module<FaqState, any> = {
  namespaced: true,
  state: {
    faqDetails: []
  },
  mutations,
  actions,
  getters
}
