import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.STORELOCATOR_FETCH] (state, storelocators) {
    state.storelocators = storelocators || []
  }
}
