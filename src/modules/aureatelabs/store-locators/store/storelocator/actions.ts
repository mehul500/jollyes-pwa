import { ActionTree } from 'vuex';
import StoreLocatorsState from '../../types/StoreLocatorsState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'
import rootStore from '@vue-storefront/core/store'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers'
import fetch from 'isomorphic-fetch'

const storeLocatorEntityName = 'aureate_storelocator';

const actions: ActionTree<StoreLocatorsState, any> = {
  /**
   * Retrieve storelocators data
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (
    context,
    {
      filterValues = null,
      filterField = 'id',
      size = 500,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { like: filterValues }
      });
    }
    if (
      skipCache ||
      !context.state.storelocators ||
      context.state.storelocators.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: storeLocatorEntityName,
        excludeFields,
        size,
        includeFields
      })
        .then(resp => {
          context.commit(types.STORELOCATOR_FETCH, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'storelocators')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.storelocators;
        resolve(resp);
      });
    }
  },
  async storeList (context, id) {
    try {
      let url = rootStore.state.config.aureatelabs.store.list.replace('{{storeId}}', id)
      url = processLocalizedURLAddress(url);
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.STORELOCATOR_FETCH, data.result)
          }
        })
    } catch (e) {
      Logger.error(e, 'stores')()
    }
  },
  async checkStock (context, payload) {
    try {
      let url = rootStore.state.config.aureatelabs.store.stock
      url = processLocalizedURLAddress(url);
      return await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      }).then(response => response.json());
    } catch (e) {
      Logger.error(e, 'stores')()
    }
  }
};
export default actions;
