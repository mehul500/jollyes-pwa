import StoreLocatorsState from '../../types/StoreLocatorsState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<StoreLocatorsState, any> = {
  getStoreLocatorsList: (state) => state.storelocators
}
