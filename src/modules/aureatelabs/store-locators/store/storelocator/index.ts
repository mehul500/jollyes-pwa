import { Module } from 'vuex'
import StoreLocatorsState from '../../types/StoreLocatorsState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const StoreLocatorsModuleStore: Module<StoreLocatorsState, any> = {
  namespaced: true,
  state: {
    storelocators: []
  },
  mutations,
  actions,
  getters
}
