import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { StoreLocatorsModuleStore } from './store/storelocator/index';

export const StoreLocatorsModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('store-locators', StoreLocatorsModuleStore)
};
