/**
  # The MIT License (MIT)

  Copyright (c) Dans Ma Culotte <contact@dansmaculotte.fr>

  > Permission is hereby granted, free of charge, to any person obtaining a copy
  > of this software and associated documentation files (the "Software"), to deal
  > in the Software without restriction, including without limitation the rights
  > to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  > copies of the Software, and to permit persons to whom the Software is
  > furnished to do so, subject to the following conditions:
  >
  > The above copyright notice and this permission notice shall be included in
  > all copies or substantial portions of the Software.
  >
  > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  > OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  > THE SOFTWARE.
 */

export default {
  install (Vue, options = {}) {
    const initAmazonConnect = () => {
      const root = new Vue()
      root._script = document.createElement('script');
      root._script.type = 'text/javascript';
      root._script.async = true;
      root._script.id = 'amazon-connect';
      root._script.src = '/assets/amazon-connect-chat-interface.js';
      root._script.setAttribute('rel', 'dns-prefetch');
      const first = document.getElementsByTagName('script')[0];
      first.parentNode.insertBefore(root._script, first);

      root._script.onload = (event) => {
        try {
          Vue.prototype.$bus.$emit('amazon-connect-start')
          window.connect.ChatInterface.init({
            containerId: 'chat_root',
            headerConfig: {
              isHTML: true,
              render: () => {
                return (`
                    <div class="header-wrapper">
                        <h2 class="welcome-text">Jollyes Customer Services</h2>
                        <p id="chatDescription">We are here to help you</p>
                    </div>
                `)
              }
            }
          });
        } catch (e) {
          console.log(e)
        }
      }
    }
    setTimeout(initAmazonConnect, options.initTimeout);
  }
};
