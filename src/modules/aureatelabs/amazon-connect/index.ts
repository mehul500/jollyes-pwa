import Vue from 'vue';
import { isServer } from '@vue-storefront/core/helpers';
import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import { amazonConnectModule } from './store'
import AmazonConnect from './VueAmazonConnectPlugin';

export const KEY = 'amazon'

export const AmazonConnectModule: StorefrontModule = async function ({ store, appConfig }) {
  if (isServer) return;
  const amazonConfig = {
    contactFlowId: 'adeb1a8d-d65e-471b-8f1f-5fd9ea6824ee',
    instanceId: 'b5e81950-ad24-4803-94ad-d44ae0c16400',
    initTimeout: (appConfig.aureatelabs.criteo.criteoTimeOut * 1000) || 0
  }
  Vue.use(AmazonConnect, amazonConfig)
  store.registerModule(KEY, amazonConnectModule)
}
