import QuickLinksState from '../../types/QuickLinksState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<QuickLinksState, any> = {
  getFooterQuickLinksList: (state) => state.quicklinks
}
