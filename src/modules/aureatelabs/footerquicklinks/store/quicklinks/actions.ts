import { ActionTree } from 'vuex';
import QuickLinksState from '../../types/QuickLinksState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'
import config from 'config';

const alQuickLinksEntityName = config.aureatelabs.es_ql_key;

const actions: ActionTree<QuickLinksState, any> = {
  /**
   * Retrieve quicklinks
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (
    context,
    {
      filterValues = null,
      filterField = 'id',
      size = 150,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { like: filterValues }
      });
    }
    if (
      skipCache ||
      !context.state.quicklinks ||
      context.state.quicklinks.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: alQuickLinksEntityName,
        excludeFields,
        includeFields
      })
        .then(resp => {
          context.commit(types.QUICKLINKS_FETCH, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'quick-links')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.quicklinks;
        resolve(resp);
      });
    }
  }
};
export default actions;
