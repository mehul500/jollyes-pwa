import exclusiveProductState from '../../types/exclusiveProductState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<exclusiveProductState, any> = {
  getExclusiveProductslist: (state) => state.exclusive
}
