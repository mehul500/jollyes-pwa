import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.EXCLUSIVE_PRODUCTS_FETCH] (state, exclusive) {
    state.exclusive = exclusive || []
  }
}
