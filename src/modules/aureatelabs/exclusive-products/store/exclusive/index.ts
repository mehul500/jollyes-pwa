import { Module } from 'vuex'
import exclusiveProductState from '../../types/exclusiveProductState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'

export const ExclusiveProductsStore: Module<exclusiveProductState, any> = {
  namespaced: true,
  state: {
    exclusive: []
  },
  mutations,
  actions,
  getters
}
