import { ActionTree } from 'vuex';
import exclusiveProductState from '../../types/exclusiveProductState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import rootStore from '@vue-storefront/core/store'
import fetch from 'isomorphic-fetch'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';

const actions: ActionTree<exclusiveProductState, any> = {
  /**
   * Retrieve exclusiveProductState
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  async getExclusiveProductslist (context, payload) {
    try {
      let url = rootStore.state.config.aureatelabs.exclusiveProducts.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      url = processLocalizedURLAddress(url);
      await fetch(url + '/' + payload, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.EXCLUSIVE_PRODUCTS_FETCH, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'exclusive-product-list')()
          }
        })
    } catch (e) {
      Logger.error(e)
    }
  }
};
export default actions;
