import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { ExclusiveProductsStore } from './store/exclusive/index';

export const ExclusiveProducts: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('exclusiveProducts', ExclusiveProductsStore)
};
