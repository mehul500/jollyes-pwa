import { Store } from 'vuex'
declare var window: any
function segmentScript (authKey, windowObject, store, route) {
  if (!windowObject.initialize) {
    if (windowObject.invoked) window.console && console.error && console.error('Segment snippet included twice.');
    else {
      windowObject.invoked = !0;
      windowObject.methods = ['trackSubmit', 'trackClick', 'trackLink', 'trackForm', 'pageview', 'identify', 'reset', 'group', 'track', 'ready', 'alias', 'debug', 'page', 'once', 'off', 'on', 'addSourceMiddleware', 'addIntegrationMiddleware', 'setAnonymousId', 'addDestinationMiddleware'];
      windowObject.factory = function (e) {
        return function () {
          var t = Array.prototype.slice.call(arguments);
          t.unshift(e);
          windowObject.push(t);
          return windowObject
        }
      };
      for (var e = 0; e < windowObject.methods.length; e++) {
        var key = windowObject.methods[e];
        windowObject[key] = windowObject.factory(key)
      }
      windowObject.load = function (key, e) {
        var t = document.createElement('script');
        t.type = 'text/javascript';
        t.async = !0;
        t.src = 'https://cdn.segment.com/analytics.js/v1/' + key + '/analytics.min.js';
        t.setAttribute('rel', 'dns-prefetch');
        var n = document.getElementsByTagName('script')[0];
        n.parentNode.insertBefore(t, n);
        windowObject._loadOptions = e
      };
      windowObject._writeKey = authKey;
      windowObject.SNIPPET_VERSION = '4.13.2';
      store.commit('segment/pageLoad', route)
      return windowObject.load(authKey);
    }
  }
}
export function beforeRegistration (app, config) {
  if (typeof window !== 'undefined') {
    window.analytics = window.analytics || [];
    window.analytics_client = window.analytics_client || [];
    if (config.segment && config.segment.authKey && config.segment.authKey !== '' && config.segment.authKey_client && config.segment.authKey_client !== '') {
      // segmentScript(config.segment.authKey, window.analytics)
      setTimeout(() => {
        segmentScript(config.segment.authKey_client, window.analytics_client, app.$store, app.$route)
      }, 10000);
    }
  }
}
