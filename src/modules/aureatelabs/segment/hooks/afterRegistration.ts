/* eslint-disable eqeqeq */
import { Store } from 'vuex'
import Analytics from 'analytics-node'
import rootStore from '@vue-storefront/core/store'
import flattenDeep from 'lodash-es/flattenDeep'
import { Logger } from '@vue-storefront/core/lib/logger'

declare var window: any
export function afterRegistration (config, store: Store<any>) {
  if (typeof window !== 'undefined') {
    const getCookie = (name) => {
      let nameEQ = name + '=';
      let ca = document.cookie.split(';');

      for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
      }
      return null;
    }
    if (config.segment && config.segment.authKey && config.segment.authKey !== '' && config.segment.authKey_client && config.segment.authKey_client !== '') {
      const analytics = new Analytics(config.segment.authKey)

      let customCate = []
      const getCategoryLevels = (currentCategory) => {
        let category_1 = ''
        let category_2 = ''
        let category_3 = currentCategory.name
        let category = rootStore.getters['category-next/getMenuCategories'];
        let levelCatetgory = flattenDeep(category.map(x => x.children_data))
        let levelCatetgory2 = flattenDeep(levelCatetgory.map(x => (typeof x != 'undefined') ? x.children_data : false))
        let finalCat = [...category, ...levelCatetgory, ...levelCatetgory2]
        let level2 = finalCat.find(y => (typeof y != 'undefined') ? y.id === currentCategory.parent_id : false)

        if (typeof level2 != 'undefined') {
          category_2 = level2.name
          let level1 = finalCat.find(x => (typeof x != 'undefined') ? x.id == level2.parent_id : false)
          if (typeof level1 != 'undefined') {
            category_1 = level1.name
          }
        }

        return { category_level_1: category_1, category_level_2: category_2, category_level_3: category_3 }
      }
      const productDetail = (state, product, categories = null) => {
        var attrubutes = state.attribute.list_by_code;
        let productDietary = []
        if (product.dietary_requirement && product.dietary_requirement.length > 0) {
          productDietary = JSON.parse(JSON.stringify(product.dietary_requirement));
        }

        var colours = attrubutes.colour ? attrubutes.colour.options.filter(y => y.value == product.colour) : (product.color) ? attrubutes.colour.options.filter(y => y.value == product.color) : [];
        var brands = attrubutes.brands ? attrubutes.brands.options.filter(y => y.value == product.brands) : [];
        var lifestage = attrubutes.life_stage ? attrubutes.life_stage.options.filter(y => y.value == product.life_stage) : [];
        var breed_size = attrubutes.breed_size ? attrubutes.breed_size.options.filter(y => y.value == product.breed_size) : [];
        var dietary_requirement = attrubutes.dietary_requirement ? attrubutes.dietary_requirement.options.filter(y => productDietary.includes(parseInt(y.value))) : [];
        var main_flavour = attrubutes.main_flavour ? attrubutes.main_flavour.options.filter(y => y.value == product.main_flavour) : [];
        var pack_weight = attrubutes.pack_weight ? attrubutes.pack_weight.options.filter(y => y.value == product.pack_weight) : [];
        let category_level_1 = ''
        let category_level_2 = ''
        let category_level_3 = ''

        if (categories === null) {
          let category = (typeof (product.category) === 'object') ? product.category : (state.product.current != null) ? state.product.current.category : []
          category_level_1 = category.length > 0 ? category[0].name : null
          category_level_2 = category.length > 1 ? category[1].name : null
          category_level_3 = category.length > 2 ? category[2].name : null
        } else {
          category_level_1 = categories.category_level_1
          category_level_2 = categories.category_level_2
          category_level_3 = categories.category_level_3
        }

        return {
          product_id: String(product.id),
          sku: product.sku,
          category_level_1: category_level_1,
          category_level_2: category_level_2,
          category_level_3: category_level_3,
          name: product.name,
          brand: (brands.length > 0) ? brands[0].label : null,
          revenue: product.price,
          price: product.price_incl_tax,
          lifestage: (lifestage.length > 0) ? lifestage[0].label : null,
          breed_size: (breed_size.length > 0) ? breed_size[0].label : null,
          dietary_needs: (dietary_requirement.length > 0) ? dietary_requirement[0].label : null,
          main_flavour: (main_flavour.length > 0) ? main_flavour[0].label : null,
          colour: (colours.length > 0) ? colours[0].label : null,
          pack_weight: (pack_weight.length > 0) ? pack_weight[0].label : null,
          quantity: product.qty,
          image_url: product.image,
          url: product.url_path
        }
      }

      const getProductList = (state, products, categories = null) => {
        let product = []
        if (products.length > 0) {
          products.forEach(x => {
            product.push(productDetail(state, x, categories))
          })
        }
        return product;
      }
      const getUserData = (data, loyalty_number) => {
        loyalty_number != null ? data.userId = loyalty_number.value : data.anonymousId = window.analytics.user().anonymousId()
        return data
      }

      store.subscribe(({ type, payload }, state) => {
        if (typeof window.analytics.initialize !== 'undefined') {
          let loyalty_number = null
          let mobile = ''
          let opt_in_email = false
          let opt_in_sms = false
          let storeData = getCookie('storelocator')
          let store_name = ''
          let store_warehouse_id = ''

          try {
            if (state.user.current != null && state.user.current.custom_attributes) {
              loyalty_number = state.user.current.custom_attributes.find((a) => { return (a.attribute_code === 'loyalty_number') });

              let mobileData = state.user.current.custom_attributes.find((a) => { return (a.attribute_code === 'mobilenumber') })
              mobile = typeof mobileData != 'undefined' ? mobileData['value'] : ''

              let emailData = state.user.current.custom_attributes.find((a) => { return (a.attribute_code === 'contact_by_email') })
              opt_in_email = typeof emailData !== 'undefined' ? emailData.value : false

              let smsData = state.user.current.custom_attributes.find((a) => { return (a.attribute_code === 'contact_by_sms') })
              opt_in_sms = typeof smsData !== 'undefined' ? smsData.value : false
            }
          } catch (ex) {
            Logger.error('Segment Attribute Missing: ' + ex)()
          }

          if (storeData != null) {
            storeData = JSON.parse(storeData)
            store_name = storeData['name']
            store_warehouse_id = storeData['store_code']
          }

          // Track for customer login Start
          if (type === 'segment/userLogin') {
            try {
              let data = {
                event: 'Signed In',
                properties: {
                  email: payload.email,
                  source: 'WEB',
                  user_id: loyalty_number != null ? loyalty_number.value : loyalty_number
                },
                traits: {
                  created_at: payload.created_at,
                  email: payload.email ? payload.email : null,
                  first_name: payload.firstname ? payload.firstname : null,
                  last_name: payload.lastname ? payload.lastname : null,
                  phone: mobile,
                  opt_in_email: Boolean(Number(opt_in_email)),
                  opt_in_sms: Boolean(Number(opt_in_sms)),
                  signup_source: 'WEB',
                  user_id: loyalty_number != null ? loyalty_number.value : loyalty_number,
                  store_name: store_name,
                  store_warehouse_id: store_warehouse_id
                }
              }

              data = getUserData(data, loyalty_number)
              window.analytics.identify(loyalty_number != null ? data['userId'] : data['anonymousId'], data.traits)

              analytics.identify(data);
              analytics.track(data)
            } catch (ex) {
              Logger.error('Segment User Login Error: ' + ex)()
            }
          }

          // Track for signup
          if (type === 'segment/userRegister') {
            try {
              let data = {
                event: 'Account Created',
                properties: {
                  email: payload.email,
                  first_name: payload.firstName,
                  last_name: payload.lastName,
                  phone: payload.mobile,
                  source: 'WEB'
                },
                traits: {
                  created_at: payload.created_at,
                  email: payload.email ? payload.email : null,
                  first_name: payload.firstName ? payload.firstName : null,
                  last_name: payload.lastName ? payload.lastName : null,
                  phone: mobile,
                  opt_in_email: Boolean(Number(opt_in_email)),
                  opt_in_sms: Boolean(Number(opt_in_sms)),
                  signup_source: 'WEB',
                  user_id: loyalty_number != null ? loyalty_number.value : loyalty_number,
                  store_name: store_name,
                  store_warehouse_id: store_warehouse_id
                }
              }

              data = getUserData(data, loyalty_number)
              window.analytics.identify(loyalty_number != null ? data['userId'] : data['anonymousId'], data.traits)
              analytics.identify(data);
              analytics.track(data)
            } catch (ex) {
              Logger.error('Segment User Register Error: ' + ex)()
            }
          }

          // Track for logout
          if (type === 'segment/userLogout') {
            try {
              let data = {
                event: 'Signed Out',
                properties: {
                  email: payload.email,
                  source: 'WEB',
                  user_id: payload.loyalty_number
                }
              }

              data = getUserData(data, loyalty_number)
              analytics.track(data)
            } catch (ex) {
              Logger.error('Segment Logout Event Error: ' + ex)()
            }
          }

          // Track for Add Product to cart
          if (type === 'cart/cart/ADD') {
            try {
              let data = {
                event: 'Product Added',
                properties: productDetail(state, payload.product)
              }
              data.properties['cart_id'] = state.cart.cartServerToken ? state.cart.cartServerToken.toString() : '';

              data = getUserData(data, loyalty_number)
              analytics.track(data);
            } catch (ex) {
              Logger.error('Segment Add Cart Error: ' + ex)()
            }
          }

          // Track for Remove Product from cart
          if (type === 'cart/cart/DEL') {
            try {
              let data = {
                event: 'Product Removed',
                properties: productDetail(state, payload.product)
              }
              data.properties['cart_id'] = state.cart.cartServerToken ? state.cart.cartServerToken.toString() : '';

              data = getUserData(data, loyalty_number)
              analytics.track(data);
            } catch (ex) {
              Logger.error('Segment Remove Cart Error: ' + ex)()
            }
          }

          // Track for Completed Place order
          if (type === 'order/orders/LAST_ORDER_CONFIRMATION' && 0) {
            try {
              let rakutenCookie = getCookie('jollyesGateway')
              const orderId = payload.confirmation.backendOrderId
              const orderData = store.getters['cart/getTotals'];

              let storeData = getCookie('storelocator')
              let coupon = '';
              let revenue = orderData.filter(x => x.code === 'grand_total');
              let discount = orderData.filter(x => x.code === 'discount');
              let shipping = orderData.filter(x => x.code === 'shipping');
              let tax = orderData.filter(x => x.code === 'tax');
              let total = orderData.filter(x => x.code === 'subtotal');

              let delivery_store = ''
              if (storeData != null) {
                storeData = JSON.parse(storeData)
                delivery_store = storeData['name']
              }

              (revenue.length > 0) ? revenue = revenue[0].value : revenue = 0;
              (shipping.length > 0) ? shipping = shipping[0].value : shipping = 0;
              (tax.length > 0) ? tax = tax[0].value : tax = 0;
              (total.length > 0) ? total = total[0].value : total = 0;
              if (discount.length > 0) {
                coupon = discount[0].title;
                discount = discount[0].value;
              } else { discount = 0 }

              let data = {
                event: 'Order Completed',
                properties: {
                  coupon: coupon,
                  currency: 'GBP',
                  delivery_method: payload.order.addressInformation.shipping_method_code,
                  delivery_store: delivery_store,
                  discount: discount,
                  order_id: orderId,
                  products: getProductList(state, payload.order.products),
                  revenue: revenue,
                  shipping: shipping,
                  tax: tax,
                  total: total
                }
              }

              if (rakutenCookie != null) {
                rakutenCookie = (rakutenCookie != null) ? decodeURI(rakutenCookie) : null
                let rakutenCookieData = (rakutenCookie != null) ? rakutenCookie.split('atrv%3A') : []
                let rakutenCookieTime = new Date().toISOString()

                data.properties['siteID'] = (rakutenCookieData.length) ? rakutenCookieData[1] : null
                data.properties['date_time'] = rakutenCookieTime
              }
              data = getUserData(data, loyalty_number)
              analytics.track(data);
            } catch (ex) {
              Logger.error('Segment Order Complete Error: ' + ex)()
            }
          }

          // Track for Completed Place order
          if (type === 'segment/orderCompleted') {
            try {
              let rakutenCookie = getCookie('jollyesGateway')
              const orderId = payload.confirmation.backendOrderId
              const orderData = payload.total;

              let storeData = getCookie('storelocator')
              let coupon = '';
              let revenue = orderData.filter(x => x.code === 'grand_total');
              let discount = orderData.filter(x => x.code === 'discount');
              let shipping = orderData.filter(x => x.code === 'shipping');
              let tax = orderData.filter(x => x.code === 'tax');
              let total = orderData.filter(x => x.code === 'subtotal');

              let delivery_store = ''
              if (storeData != null) {
                storeData = JSON.parse(storeData)
                delivery_store = storeData['name']
              }

              (revenue.length > 0) ? revenue = revenue[0].value : revenue = 0;
              (shipping.length > 0) ? shipping = shipping[0].value : shipping = 0;
              (tax.length > 0) ? tax = tax[0].value : tax = 0;
              (total.length > 0) ? total = total[0].value : total = 0;
              if (discount.length > 0) {
                coupon = discount[0].title;
                discount = discount[0].value;
              } else { discount = 0 }

              let data = {
                event: 'Order Completed',
                properties: {
                  coupon: coupon,
                  currency: 'GBP',
                  delivery_method: payload.order.addressInformation.shipping_method_code,
                  delivery_store: delivery_store,
                  discount: discount,
                  order_id: orderId,
                  products: getProductList(state, payload.order.products),
                  revenue: revenue,
                  shipping: shipping,
                  tax: tax,
                  total: total
                }
              }

              if (rakutenCookie != null) {
                rakutenCookie = (rakutenCookie != null) ? decodeURI(rakutenCookie) : null
                let rakutenCookieData = (rakutenCookie != null) ? rakutenCookie.split('atrv%3A') : []
                let rakutenCookieTime = new Date().toISOString()

                data.properties['siteID'] = (rakutenCookieData.length) ? rakutenCookieData[1] : null
                data.properties['date_time'] = rakutenCookieTime
              }
              data = getUserData(data, loyalty_number)
              analytics.track(data);
            } catch (ex) {
              Logger.error('Segment Order Complete Error: ' + ex)()
            }
          }

          // Track for Cart View
          if (type === 'ui/cartViewed') {
            try {
              let data = {
                cart_id: state.cart.cartServerToken ? state.cart.cartServerToken.toString() : '',
                products: getProductList(state, JSON.parse(localStorage.getItem('shop/cart/current-cart')))
              }
              if (loyalty_number != null) {
                data['userId'] = loyalty_number.value
              }
              window.analytics.track('Cart Viewed', data);
            } catch (ex) {
              Logger.error('Segment Cart View Error: ' + ex)()
            }
          }

          // Track for Product View
          if (type === 'segment/produtView') {
            try {
              let curretnCategory = rootStore.getters['breadcrumbs/getBreadcrumbsRoutes']
              let data = productDetail(state, payload)
              data = getUserData(data, loyalty_number)

              window.analytics.track('Product Viewed', data);

              let pageData = {
                category_level_1: (curretnCategory.length >= 1) ? payload.category[0].name : '',
                category_level_2: (payload.category.length >= 2) ? payload.category[1].name : '',
                category_level_3: (payload.category.length >= 3) ? payload.category[2].name : '',
                title: payload.name
              }

              window.analytics.page(payload.name, pageData);
            } catch (ex) {
              Logger.error('Segment Product View Error: ' + ex)()
            }
          }

          // Track for Product List
          if (type === 'segment/productListView') {
            try {
              let categories = getCategoryLevels(payload.category)
              let data = {
                products: getProductList(state, payload.products, categories),
                cart_id: state.cart.cartServerToken ? state.cart.cartServerToken.toString() : null,
                category_level_1: categories.category_level_1,
                category_level_2: categories.category_level_2,
                category_level_3: categories.category_level_3
              }

              window.analytics.track('Product List Viewed', data);

              let pageData = {
                category_level_1: data.category_level_1,
                category_level_2: data.category_level_2,
                category_level_3: data.category_level_3,
                title: payload.category.name
              }
              window.analytics.page(payload.category.name, pageData);
            } catch (ex) {
              Logger.error('Segment Product List View Error: ' + ex)()
            }
          }

          // Track for Product Clicked
          if (type === 'segment/productClicked') {
            let data = productDetail(state, payload)
            data['cart_id'] = state.cart.cartServerToken ? state.cart.cartServerToken.toString() : null;
            window.analytics.track('Product Clicked', data);
          }

          if (type === 'segment/saveAddress') {
            try {
              let data = {
                traits: {
                  created_at: payload.created_at,
                  email: payload.email ? payload.email : null,
                  first_name: payload.firstname ? payload.firstname : null,
                  last_name: payload.lastname ? payload.lastname : null,
                  phone: mobile,
                  opt_in_email: Boolean(Number(opt_in_email)),
                  opt_in_sms: Boolean(Number(opt_in_sms)),
                  signup_source: 'WEB',
                  user_id: loyalty_number != null ? loyalty_number.value : loyalty_number,
                  store_name: store_name,
                  store_warehouse_id: store_warehouse_id
                }
              }

              data = getUserData(data, loyalty_number)
              window.analytics.identify(loyalty_number != null ? data['userId'] : data['anonymousId'], data.traits);
              analytics.identify(data);
            } catch (ex) {
              Logger.error('Segment Save Address Error: ' + ex)()
            }
          }

          if (type === 'ui/setAuthElem') {
            try {
              let page = null;
              if (payload === 'register') {
                page = 'Signup'
              } else if (payload === 'login') {
                page = 'Login'
              }

              if (page != null) {
                let data = { title: page }
                data = getUserData(data, loyalty_number)
                window.analytics.page(page, data);
              }
            } catch (ex) {
              Logger.error('Segment Login Register Page View Error: ' + ex)()
            }
          }

          if (type === 'route/ROUTE_CHANGED') {
            try {
              if (payload.to.name === 'locator') {
                window.analytics.page('locator', { title: payload.to.name })
              } else if (payload.to.name === 'store') {
                window.analytics.page('store', { title: payload.to.params.storeId }, {
                  store: payload.to.params.storeId
                });
              } else if (payload.to.name === 'home') {
                window.analytics.page('Home Page', { title: 'Home Page' })
              } else {
                if (!payload.to.name.includes('urldispatcher')) {
                  window.analytics.page(payload.to.name, { title: payload.to.name })
                }
              }

              if (state.user.current != null) {
                let data = {
                  traits: {
                    created_at: state.user.current != null ? state.user.current.created_at : payload.created_at,
                    email: state.user.current != null ? state.user.current.email : null,
                    first_name: state.user.current != null ? state.user.current.firstname : null,
                    last_name: state.user.current != null ? state.user.current.lastname : null,
                    phone: mobile,
                    opt_in_email: Boolean(Number(opt_in_email)),
                    opt_in_sms: Boolean(Number(opt_in_sms)),
                    signup_source: 'WEB',
                    user_id: loyalty_number != null ? loyalty_number.value : loyalty_number,
                    store_name: store_name,
                    store_warehouse_id: store_warehouse_id
                  }
                }

                data = getUserData(data, loyalty_number)
                window.analytics.identify(loyalty_number != null ? data['userId'] : data['anonymousId'], data.traits)
                analytics.identify(data);
              }
            } catch (ex) {
              Logger.error('Segment Page View Error: ' + ex)()
            }
          }

          if (type === 'segment/pageLoad') {
            try {
              let title = ''
              if (payload.params.slug) {
                title = payload.params.slug
              } else if (payload.name === 'store' && payload.params.storeId) {
                title = payload.params.storeId
              } else {
                title = payload.name
              }

              if (!payload.name.includes('urldispatcher')) {
                window.analytics.page((payload.name === 'cms-page') ? title : payload.name, { title: title })
                if (state.user.current != null) {
                  let data = {
                    traits: {
                      created_at: state.user.current != null ? state.user.current.created_at : payload.created_at,
                      email: state.user.current != null ? state.user.current.email : null,
                      first_name: state.user.current != null ? state.user.current.firstname : null,
                      last_name: state.user.current != null ? state.user.current.lastname : null,
                      phone: mobile,
                      opt_in_email: Boolean(Number(opt_in_email)),
                      opt_in_sms: Boolean(Number(opt_in_sms)),
                      signup_source: 'WEB',
                      user_id: loyalty_number != null ? loyalty_number.value : loyalty_number,
                      store_name: store_name,
                      store_warehouse_id: store_warehouse_id
                    }
                  }

                  data = getUserData(data, loyalty_number)
                  window.analytics.identify(loyalty_number != null ? data['userId'] : data['anonymousId'], data.traits)
                  analytics.identify(data);
                }
              }
            } catch (ex) {
              Logger.error('Segment Page Load Error: ' + ex)()
            }
          }
        }
      })
    }
  }
}
