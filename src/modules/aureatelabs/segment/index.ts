import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import { segmentModule } from './store'
import { beforeRegistration } from './hooks/beforeRegistration'
import { afterRegistration } from './hooks/afterRegistration'

export const KEY = 'segment'

export const SegmentModule: StorefrontModule = function ({ app, store, appConfig, router }) {
  store.registerModule(KEY, segmentModule)
  beforeRegistration(app, appConfig)
  afterRegistration(appConfig, store)
}
