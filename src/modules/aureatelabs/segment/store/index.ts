import { Module } from 'vuex'
import SegmentState from '../types/SegmentState'

export const segmentModule: Module<SegmentState, any> = {
  namespaced: true,
  mutations: {
    userLogin (state, payload) {
    },
    userRegister (state, payload) {
    },
    userForgetPassword (state, payload) {
    },
    userLogout (state, payload) {
    },
    productListView (state, payload) {
    },
    orderCompleted (state, payload) {
    },
    produtView (state, payload) {
    },
    productClicked (state, payload) {
    },
    saveAddress (state, payload) {
    },
    pageLoad (state, payload) {
    }
  },
  state: {
    deviceLocation: {}
  }
}
