import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.OUR_BRANDS_FETCH] (state, ourbrands) {
    state.ourbrands = ourbrands || []
  }
}
