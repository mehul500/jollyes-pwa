import { ActionTree } from 'vuex';
import ourBrandState from '../../types/ourBrandState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';
import rootStore from '@vue-storefront/core/store'
import fetch from 'isomorphic-fetch'

const actions: ActionTree<ourBrandState, any> = {
  /**
   * Retrieve brands
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  async getOurBrandslist (context, payload) {
    try {
      let url = rootStore.state.config.aureatelabs.ourBrand.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      url = processLocalizedURLAddress(url);
      Logger.info('Magento 2 REST API Request with Request Data', 'our-brand-list')()
      await fetch(url + '/' + payload, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Magento 2 REST API Response Data', 'our-brand-list', { data })()
            context.commit(types.OUR_BRANDS_FETCH, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'our-brand-list')()
          }
        })
    } catch (e) {
      Logger.error(e, 'our-brand-list')()
    }
  }
};
export default actions;
