import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { addressBookModuleStore } from './store/index';

export const addressBook: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('address-book', addressBookModuleStore)
};
