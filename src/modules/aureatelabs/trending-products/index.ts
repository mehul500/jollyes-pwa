import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { TrendingProductsStore } from './store/trending/index';

export const TrendingProducts: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('trendingProducts', TrendingProductsStore)
};
