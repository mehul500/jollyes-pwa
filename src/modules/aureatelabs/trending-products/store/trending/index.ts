import { Module } from 'vuex'
import trendingProductState from '../../types/trendingProductState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'

export const TrendingProductsStore: Module<trendingProductState, any> = {
  namespaced: true,
  state: {
    trending: []
  },
  mutations,
  actions,
  getters
}
