import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.TRENDING_PRODUCTS_FETCH] (state, trending) {
    state.trending = trending || []
  }
}
