import trendingProductState from '../../types/trendingProductState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<trendingProductState, any> = {
  getTrendingProductslist: (state) => state.trending
}
