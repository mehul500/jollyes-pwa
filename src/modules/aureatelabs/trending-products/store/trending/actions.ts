import { ActionTree } from 'vuex';
import trendingProductState from '../../types/trendingProductState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import rootStore from '@vue-storefront/core/store'
import fetch from 'isomorphic-fetch'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';

const actions: ActionTree<trendingProductState, any> = {
  /**
   * Retrieve trendingProductState
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  async getTrendingProductslist (context, payload) {
    try {
      let url = rootStore.state.config.aureatelabs.trendingProducts.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      url = processLocalizedURLAddress(url);
      Logger.info('Magento 2 REST API Request with Request Data', 'trending-product-list')()
      await fetch(url + '/' + payload, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Magento 2 REST API Response Data', 'trending-product-list', { data })()
            context.commit(types.TRENDING_PRODUCTS_FETCH, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'trending-product-list')()
          }
        })
    } catch (e) {
      Logger.error(e, 'trending-product-list')()
    }
  }
};
export default actions;
