import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { AddressyModuleStore } from './store/index';

export const AddressyModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('addressy', AddressyModuleStore)
};
