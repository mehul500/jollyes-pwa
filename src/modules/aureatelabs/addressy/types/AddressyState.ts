export default interface AddressyState {
  addresses: any[],
  billingAddresses: any[],
  address: any[],
  billingAddress: any[]
}
