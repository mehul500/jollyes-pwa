import { ActionTree } from 'vuex';
import AddressyState from '../types/AddressyState';
import rootStore from '@vue-storefront/core/store'
import { Logger } from '@vue-storefront/core/lib/logger';
import * as types from './mutation-types';
import fetch from 'isomorphic-fetch';
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';

const actions: ActionTree<AddressyState, any> = {
  /**
   * Retrieve Addresses by text
   *
   * @param context
   * @param {any} keyword
   * @returns {Promise<T> & Promise<any>}
   */
  async search (context, payload) {
    try {
      let url = processLocalizedURLAddress(rootStore.state.config.aureatelabs.addressy.find)
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.ADDRESSY_SEARCH_ADDRESSES, data.result);
          }
        })
    } catch (e) {
      Logger.error(e, 'addressy')()
    }
  },
  /**
   * Retrieve Addresses by text
   *
   * @param context
   * @param {any} keyword
   * @returns {Promise<T> & Promise<any>}
   */
  async searchBilling (context, payload) {
    try {
      let url = processLocalizedURLAddress(rootStore.state.config.aureatelabs.addressy.find)
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.ADDRESSY_SEARCH_BILLING_ADDRESSES, data.result);
          }
        })
    } catch (e) {
      Logger.error(e, 'addressy')()
    }
  },

  /**
   * Retrieve Address by Id
   *
   * @param context
   * @param {any} address
   * @returns {Promise<T> & Promise<any>}
   */
  async get (context, address) {
    try {
      let url = processLocalizedURLAddress(rootStore.state.config.aureatelabs.addressy.retrieve.replace('{{addressId}}', address))
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.ADDRESSY_FETCH_ADDRESS_DETAILS, data.result);
          }
        })
    } catch (e) {
      Logger.error(e, 'addressy')()
    }
  },

  /**
   * Retrieve Billing Address by Id
   *
   * @param context
   * @param {any} address
   * @returns {Promise<T> & Promise<any>}
   */
  async getBilling (context, address) {
    try {
      let url = processLocalizedURLAddress(rootStore.state.config.aureatelabs.addressy.retrieve.replace('{{addressId}}', address))
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.ADDRESSY_FETCH_BILLING_ADDRESS_DETAILS, data.result);
          }
        })
    } catch (e) {
      Logger.error(e, 'addressy')()
    }
  },

  async clear (context) {
    context.commit(types.ADDRESSY_SEARCH_ADDRESSES, []);
  },
  async clearBilling (context) {
    context.commit(types.ADDRESSY_SEARCH_BILLING_ADDRESSES, []);
  }
};
export default actions;
