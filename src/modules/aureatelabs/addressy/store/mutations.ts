import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.ADDRESSY_SEARCH_ADDRESSES] (state, addresses) {
    state.addresses = addresses || []
  },
  [types.ADDRESSY_SEARCH_BILLING_ADDRESSES] (state, billingAddresses) {
    state.billingAddresses = billingAddresses || []
  },
  [types.ADDRESSY_FETCH_ADDRESS_DETAILS] (state, address) {
    state.address = (address[0] ? address[0] : []) || []
  },
  [types.ADDRESSY_FETCH_BILLING_ADDRESS_DETAILS] (state, billingAddress) {
    state.billingAddress = (billingAddress[0] ? billingAddress[0] : []) || []
  }
}
