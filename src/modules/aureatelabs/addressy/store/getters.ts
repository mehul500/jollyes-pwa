import AddressyState from '../types/AddressyState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<AddressyState, any> = {
  getAddressList: (state) => state.addresses,
  getBillingAddressList: (state) => state.billingAddresses,
  getAddress: (state) => state.address,
  getBillingAddress: (state) => state.billingAddress
}
