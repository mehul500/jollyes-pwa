import { Module } from 'vuex'
import AddressyState from '../types/AddressyState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const AddressyModuleStore: Module<AddressyState, any> = {
  namespaced: true,
  state: {
    addresses: [],
    billingAddresses: [],
    address: [],
    billingAddress: []
  },
  mutations,
  actions,
  getters
}
