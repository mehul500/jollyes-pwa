import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { PetClubStore } from './store/petclubs/index';

export const PetClubs: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('PetClubs', PetClubStore)
};
