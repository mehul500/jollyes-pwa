export default interface PetClubState {
  petclubpoints: any[],
  petclubvouchers: any[],
  reedeemvoucher: any[],
  removevoucher: any[],
  newsletterSubscribe: boolean
}
