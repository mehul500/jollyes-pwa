import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.PETCLUBPOINTS_FETCH] (state, petclubpoints) {
    state.petclubpoints = petclubpoints || []
  },
  [types.PETCLUBVOUCHERS_FETCH] (state, petclubvouchers) {
    state.petclubvouchers = petclubvouchers || []
  },
  [types.REDEEM_PETCLUBVOUCHERS_FETCH] (state, reedeemvoucher) {
    state.reedeemvoucher = reedeemvoucher || []
  },
  [types.REMOVE_PETCLUBVOUCHERS_FETCH] (state, removevoucher) {
    state.removevoucher = removevoucher || []
  },
  [types.SET_NEWSLETTERSUBSCRIBE_FETCH] (state, newsletter) {
    state.newsletterSubscribe = newsletter || false
  }
}
