import { Module } from 'vuex'
import PetClubState from '../../types/petClubState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'

export const PetClubStore: Module<PetClubState, any> = {
  namespaced: true,
  state: {
    petclubpoints: [],
    petclubvouchers: [],
    reedeemvoucher: [],
    removevoucher: [],
    newsletterSubscribe: false
  },
  mutations,
  actions,
  getters
}
