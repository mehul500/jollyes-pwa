import PetClubState from '../../types/petClubState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<PetClubState, any> = {
  getpetClubPointslist: (state) => state.petclubpoints,
  getpetClubVoucherslist: (state) => state.petclubvouchers,
  redeemPetClubPoitsVoucher: (state) => state.reedeemvoucher,
  removePetClubVoucher: (state) => state.removevoucher,
  getnewsletterSubscribe: (state) => state.newsletterSubscribe
}
