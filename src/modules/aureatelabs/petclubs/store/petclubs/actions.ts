import { ActionTree } from 'vuex';
import PetClubState from '../../types/petClubState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import getApiEndpointUrl from '@vue-storefront/core/helpers/getApiEndpointUrl';
import config from 'config'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers'
import fetch from 'isomorphic-fetch'

const actions: ActionTree<PetClubState, any> = {
  /**
   * Retrieve PetClubState
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  async getpetClubPointslist (context, payload) {
    try {
      const url = processLocalizedURLAddress(getApiEndpointUrl(config.aureatelabs.petclubpoints, 'endpoint'))
      await fetch(url + '/' + payload, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('API Response Data', 'pet-clubs', { data })()
            context.commit(types.PETCLUBPOINTS_FETCH, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
    }
  },
  async getpetClubVoucherslist (context, payload) {
    try {
      // let url = rootStore.state.config.aureatelabs.petclubvouchers.endpoint
      const url = processLocalizedURLAddress(getApiEndpointUrl(config.aureatelabs.petclubvouchers, 'endpoint'))
      await fetch(url + '/' + payload, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Vouchers Response Data', 'pet-clubs', { data })()
            context.commit(types.PETCLUBVOUCHERS_FETCH, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
    }
  },
  async redeemPetClubPoitsVoucher (context, payload) {
    try {
      // let url = rootStore.state.config.aureatelabs.petclubvouchers.endpoint
      const url = processLocalizedURLAddress(getApiEndpointUrl(config.aureatelabs.redeemVouchers, 'endpoint'))
      const cart_id = payload.cart_id
      const voucher_code = payload.voucher_code
      await fetch(url + '/' + cart_id + '/' + voucher_code, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Redeem Voucher Response Data', 'pet-clubs', { data })()
            context.commit(types.REDEEM_PETCLUBVOUCHERS_FETCH, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
    }
  },
  async removePetClubVoucher (context, payload) {
    try {
      // let url = rootStore.state.config.aureatelabs.petclubvouchers.endpoint
      const url = processLocalizedURLAddress(getApiEndpointUrl(config.aureatelabs.removeVouchers, 'endpoint'))
      const cart_id = payload.cart_id
      const voucher_code = payload.voucher_code
      await fetch(url + '/' + cart_id + '/' + voucher_code, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Remove Voucher Response Data', 'pet-clubs', { data })()
            context.commit(types.REMOVE_PETCLUBVOUCHERS_FETCH, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
    }
  },
  async newsletterSubscribe (context, payload) {
    try {
      const url = config.aureatelabs.newsletter.subscribe;
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Newsletter Sybscriber', 'pet-clubs', { data })()
            context.commit(types.SET_NEWSLETTERSUBSCRIBE_FETCH, true)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
    }
  },
  resetNewsletterSubscribe (context, payload) {
    try {
      context.commit(types.SET_NEWSLETTERSUBSCRIBE_FETCH, {})
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'pet-clubs')()
    }
  }
};
export default actions;
