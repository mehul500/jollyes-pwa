import { Store } from 'vuex'
declare var window: any

const addCriteoScript = (accountId) => (function (w, d, u, h, a) {
  h = d.getElementsByTagName('head')[0];
  a = d.createElement('script');
  a.async = 1;
  a.src = u
  a.type = 'text/plain'
  a.setAttribute('data-cookiefirst-category', 'advertising')
  h.appendChild(a);
})(window as any, document, '//dynamic.criteo.com/js/ld/ld.js?a=' + accountId);

export function beforeRegistration (config, store: Store<any>) {
  if (typeof window !== 'undefined') {
    addCriteoScript(config.aureatelabs.criteo.criteo_ac_id)
    if (typeof window.criteo_q !== 'undefined') {
      window.criteo_q = undefined
    }
  }
}
