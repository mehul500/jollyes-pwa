import { Store } from 'vuex'
import CryptoJS from 'crypto-js'
declare var window: any

export function afterRegistration (config, store: Store<any>) {
  store.subscribe(({ type, payload }, state) => {
    if (type === 'order/orders/LAST_ORDER_CONFIRMATION') {
      const userEmail = CryptoJS.MD5(payload.order.addressInformation.shippingAddress.email);
      const zipCode = payload.order.addressInformation.shippingAddress.postcode
      const transactionID = payload.confirmation.orderNumber

      let itemsAry = []
      for (var i = 0; i < payload.order.products.length; i++) {
        itemsAry.push(
          {
            id: payload.order.products[i].id,
            price: payload.order.products[i].priceInclTax,
            quantity: payload.order.products[i].qty
          }
        )
      }

      window.criteo_q = window.criteo_q || [];
      let deviceType = /iPad/.test(navigator.userAgent) ? 't' : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? 'm' : 'd';
      window.criteo_q.push(
        { event: 'setAccount', account: config.aureatelabs.criteo.criteo_ac_id },
        { event: 'setEmail', email: userEmail.toString() },
        { event: 'setZipcode', zipcode: zipCode },
        { event: 'setSiteType', type: deviceType },
        { event: 'trackTransaction',
          id: transactionID,
          item: itemsAry
        }
      );
    }
  })
}
