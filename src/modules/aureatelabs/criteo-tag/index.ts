import Vue from 'vue';
import { isServer } from '@vue-storefront/core/helpers';
import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import { criteoModule } from './store'
import Criteo from './VueCriteoPlugin';
import { afterRegistration } from './hooks/afterRegistration'

export const KEY = 'criteo'

export const CriteoModule: StorefrontModule = async function ({ store, appConfig }) {
  if (isServer) return;
  const criteoConfig = {
    criteoAccId: appConfig.aureatelabs.criteo.criteo_ac_id,
    initTimeout: (appConfig.aureatelabs.criteo.criteoTimeOut * 1000) || 0
  }
  Vue.use(Criteo, criteoConfig)
  store.registerModule(KEY, criteoModule)
  // beforeRegistration(appConfig, store)
  afterRegistration(appConfig, store)
}
