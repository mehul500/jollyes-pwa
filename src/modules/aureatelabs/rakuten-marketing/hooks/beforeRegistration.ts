import { Store } from 'vuex'
declare var window: any
declare var DataLayer: any

const loadRakutenScript = () => (function (w, d, u, h, a) {
  h = d.getElementsByTagName('head')[0];
  a = d.createElement('script');
  a.async = 1;
  a.src = u
  a.type = 'text/plain'
  a.setAttribute('data-cookiefirst-category', 'advertising')
  a.setAttribute('rel', 'dns-prefetch');
  h.appendChild(a);
})(window as any, document, '//tag.rmp.rakuten.com/123075.ct.js');

export function beforeRegistration (config, store: Store<any>) {
  if (typeof window !== 'undefined') {
    if (!window.DataLayer) {
      window.DataLayer = {};
    }
    if (!DataLayer.events) {
      DataLayer.events = {};
    }
    DataLayer.events.SPIVersion = DataLayer.events.SPIVersion || '3.4.1';
    DataLayer.events.SiteSection = '1';
    loadRakutenScript();
  }
}
