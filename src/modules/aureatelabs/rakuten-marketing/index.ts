import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import { rakutenModule } from './store'
import { beforeRegistration } from './hooks/beforeRegistration'
import { afterRegistration } from './hooks/afterRegistration'

export const KEY = 'rakuten'

export const RakutenModule: StorefrontModule = function ({ store, appConfig }) {
  store.registerModule(KEY, rakutenModule)
  beforeRegistration(appConfig, store)
  afterRegistration(appConfig, store)
}
