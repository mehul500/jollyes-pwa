import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.BRAND_FETCH_BRAND_LIST] (state, brands) {
    state.brands = brands || []
  },
  [types.BRAND_FETCH_BRAND_DETAILS] (state, brand) {
    state.brand = (brand[0] ? brand[0] : []) || []
  },
  [types.BRAND_FETCH_BRAND_PRODUCTS] (state, products) {
    state.products = products || []
  }
}
