import BannerState from '../../types/BrandState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<BannerState, any> = {
  getBrandList: (state) => state.brands,
  getBrand: (state) => state.brand
}
