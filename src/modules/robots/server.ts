import { serverHooks } from '@vue-storefront/core/server/hooks'
const path = require('path')
serverHooks.afterApplicationInitialized(({ app }) => {
  app.get('/robots.txt', (req, res) => {
    res.end('User-agent: *\nDisallow: /i/cookie-policy\nDisallow: /i/privacy-policy\nDisallow: /terms-and-conditions\nDisallow: /i/responsible-retailing\nDisallow: /assets\nDisallow: /dist\nDisallow: /my-account\nDisallow: /nsearch\nDisallow: /ca\nSitemap: https://www.jollyes.co.uk/media/sitemap.xml')
  })
  app.get('/webpush-service-worker.js', (req, res) => {
    res.sendFile(path.join(__dirname, '../../../webpush-service-worker.js'))
  })
})
