import { VueStorefrontModule } from '@vue-storefront/core/lib/module'
import { CatalogModule } from '@vue-storefront/core/modules/catalog'
import { CatalogNextModule } from '@vue-storefront/core/modules/catalog-next'
import { CartModule } from '@vue-storefront/core/modules/cart'
import { CheckoutModule } from '@vue-storefront/core/modules/checkout'
import { CompareModule } from '@vue-storefront/core/modules/compare'
import { WishlistModule } from '@vue-storefront/core/modules/wishlist'
import { NotificationModule } from '@vue-storefront/core/modules/notification'
import { UrlModule } from '@vue-storefront/core/modules/url'
import { BreadcrumbsModule } from '@vue-storefront/core/modules/breadcrumbs'
import { UserModule } from '@vue-storefront/core/modules/user'
import { CmsModule } from '@vue-storefront/core/modules/cms'
import { NewsletterModule } from '@vue-storefront/core/modules/newsletter'
import { InitialResourcesModule } from '@vue-storefront/core/modules/initial-resources'
import { BannerModule } from './aureatelabs/banners'
import { StoreLocatorsModule } from './aureatelabs/store-locators'
import { BrandModule } from './aureatelabs/brands'
import { OurBrandModule } from './aureatelabs/brands-by-category'
import { TrendingProducts } from './aureatelabs/trending-products'
import { ExclusiveProducts } from './aureatelabs/exclusive-products'
import { PetClubs } from './aureatelabs/petclubs'
import { QuickLinksModule } from './aureatelabs/footerquicklinks'
import { PaymentAdyen } from './adyen'
import { addressBook } from './aureatelabs/addres-book'
import { registerModule } from '@vue-storefront/core/lib/modules'
import { BestSellingModule } from './aureatelabs/best-selling'
import { GoogleRecaptchaModule } from './google-recaptcha';
import { StoresModule } from './aureatelabs/store-location'
import { SegmentModule } from './aureatelabs/segment'
import { CriteoModule } from './aureatelabs/criteo-tag'
import { AmazonConnectModule } from './aureatelabs/amazon-connect'
import { RakutenModule } from './aureatelabs/rakuten-marketing'
import { FaqModule } from './aureatelabs/faq'
import { AddressyModule } from './aureatelabs/addressy'
import { CookieconsentModule } from './aureatelabs/cookie-consent'
import { PaginationModule } from './vue-storefront-pagination'

// TODO:distributed across proper pages BEFORE 1.11
export function registerClientModules () {
  registerModule(UrlModule)
  registerModule(CatalogModule)
  registerModule(CheckoutModule) // To Checkout
  registerModule(CartModule)
  registerModule(WishlistModule) // Trigger on wishlist icon click
  registerModule(NotificationModule)
  registerModule(UserModule) // Trigger on user icon click
  registerModule(CatalogNextModule)
  registerModule(CompareModule)
  registerModule(BreadcrumbsModule)
  registerModule(CmsModule)
  registerModule(NewsletterModule)
  registerModule(InitialResourcesModule)
  registerModule(addressBook)
  registerModule(BannerModule)
  registerModule(StoreLocatorsModule)
  registerModule(BrandModule)
  registerModule(BestSellingModule)
  registerModule(OurBrandModule)
  registerModule(TrendingProducts)
  registerModule(ExclusiveProducts)
  registerModule(PetClubs)
  registerModule(QuickLinksModule)
  registerModule(StoresModule)
  registerModule(GoogleRecaptchaModule)
  registerModule(SegmentModule)
  registerModule(CriteoModule)
  registerModule(AmazonConnectModule)
  registerModule(RakutenModule)
  registerModule(FaqModule)
  registerModule(AddressyModule)
  registerModule(CookieconsentModule)
  registerModule(PaginationModule)
}
// Deprecated API, will be removed in 2.0
export const registerModules: VueStorefrontModule[] = [
  // Example
  PaymentAdyen
]
