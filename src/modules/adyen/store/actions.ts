import { AdyenState, AuthDetails } from '../types/AdyenState';
import { ActionTree } from 'vuex';
import { Logger } from '@vue-storefront/core/lib/logger';
import fetch from 'isomorphic-fetch';
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';
import * as types from './mutation-types';

export const actions: ActionTree<AdyenState, any> = {
  async loadPaymentMethods ({ commit, rootGetters }, payload) {
    const cartId = rootGetters['cart/getCartToken'];
    if (!cartId && payload && !payload.listing) {
      console.error('[Adyen] CartId does not exist');
      return;
    }

    const baseUrl = processLocalizedURLAddress(
      '/api/ext/adyen/getPaymentMethods'
    );

    try {
      let token = '';
      if (rootGetters['user/getUserToken']) {
        token = `?token=${rootGetters['user/getUserToken']}`;
      }

      let response = await fetch(baseUrl, {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
          'Content-Type': 'application/json'
        }
      });

      let { result } = await response.json();
      return result || {};
    } catch (err) {
      Logger.error('[Adyen getPaymentMethods]', err);
    }
  },
  async removePaymentCards ({ commit, rootGetters }, payload) {
    const baseUrl = processLocalizedURLAddress(
      '/api/ext/adyen/removeStoredPaymentCard'
    );

    try {
      let response = await fetch(baseUrl, {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
          'Content-Type': 'application/json'
        }
      });

      let { result } = await response.json();
      return result || {};
    } catch (err) {
      Logger.error('[Adyen getPaymentMethods]', err);
    }
  },

  async initiatePayment ({ commit, rootGetters }, payload) {
    payload.state.data.currencyCode = payload.currency;
    payload.state.data.amount = payload.amount;
    payload.state.data.orderRef = payload.orderRef;
    payload.state.data.countryCode = payload.countryCode ? payload.countryCode : 'UK';
    if (payload.state.data.paymentMethod.type !== 'klarna' && payload.state.data.paymentMethod.type !== 'klarna_account') {
      payload.state.data.countryCode = 'UK';
    }
    payload.state.data.billingAddress = payload.billingAddress;
    payload.state.data.deliveryAddress = payload.deliveryAddress;
    payload.state.data.shopperName = payload.shopperName;
    payload.state.data.shopperEmail = payload.shopperEmail;
    payload.state.data.shopperNumber = payload.shopperNumber;
    payload.state.data.shopperReference = payload.shopperReference;
    if (payload.state.data.paymentMethod.type === 'klarna' || payload.state.data.paymentMethod.type === 'klarna_account') {
      payload.state.data.returnUrl = processLocalizedURLAddress(payload.returnUrl);
      payload.state.data.lineItems = payload.lineItems || [];
      // payload.state.data.additionalData = payload.additionalData || {};
    }

    let url = processLocalizedURLAddress('/api/ext/adyen/initiatePayment');

    try {
      let response = await fetch(url, {
        method: 'POST',
        body: payload.state.data ? JSON.stringify(payload.state.data) : '',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let result = await response.json();
      if (result.result.action !== undefined && (result.result.action.paymentMethodType === 'klarna' || result.result.action.paymentMethodType === 'klarna_account')) {
        commit(types.ADYEN_PAYMENT_DATA, result.result);
      } else {
        commit(types.ADYEN_PAYMENT_DATA, {
          'merchantReference': result.result.merchantReference,
          'pspReference': result.result.pspReference,
          'resultCode': result.result.resultCode
        });
      }
      return result || {};
    } catch (err) {
      Logger.error('[Adyen initiatePayment]', err);
    }
  },

  async additionalPaymentDetail ({ commit, rootGetters }, payload) {
    let url = processLocalizedURLAddress('/api/ext/adyen/additionPaymentDetails')
    try {
      let response = await fetch(url, {
        method: 'POST',
        body: payload ? JSON.stringify(payload) : '',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let result = await response.json();
      commit(types.ADYEN_PAYMENT_DATA, {
        'merchantReference': result.result.merchantReference,
        'pspReference': result.result.pspReference,
        'resultCode': result.result.resultCode
      });
      return result || {}
    } catch (err) {
      Logger.error('[Adyen additionalPaymentDetail]', err);
    }
  },

  async invoice ({ commit, rootGetters }, payload) {
    let url = processLocalizedURLAddress('/api/ext/adyen/invoice')
    try {
      let response = await fetch(url, {
        method: 'POST',
        body: payload ? JSON.stringify(payload) : '',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let result = await response.json();
      return result || {}
    } catch (err) {
      Logger.error('[Adyen additionalPaymentDetail]', err);
    }
  },

  async track ({ commit, rootGetters }, payload) {
    let url = processLocalizedURLAddress('/api/ext/adyen/track')
    try {
      let response = await fetch(url, {
        method: 'POST',
        body: payload ? JSON.stringify(payload) : '',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let result = await response.json();
      return result || {}
    } catch (err) {
      Logger.error('[Adyen additionalPaymentDetail]', err);
    }
  }
};
