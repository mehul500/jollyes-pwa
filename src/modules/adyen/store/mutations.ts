import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.ADYEN_PAYMENT_DATA] (state, paymentData) {
    state.paymentData = paymentData || {}
  }
}
