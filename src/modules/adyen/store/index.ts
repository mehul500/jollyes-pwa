import { Module } from 'vuex'
import { AdyenState } from '../types/AdyenState'
import { actions } from './actions'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'

export const module: Module<AdyenState, any> = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
