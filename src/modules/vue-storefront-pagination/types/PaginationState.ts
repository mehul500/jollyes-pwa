export default interface PaginationState {
  products: any[],
  category: number,
  filters: any[]
}
