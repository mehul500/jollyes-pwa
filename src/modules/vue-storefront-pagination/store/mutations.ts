import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_CATEGORY_PRODUCTS] (state, products) {
    state.products = products || []
  },
  [types.FETCH_CURRENT_CATEGORY] (state, category) {
    state.category = category || 0
  },
  [types.CLEAR_CATEGORY_PRODUCTS] (state) {
    state.products = []
  },
  [types.FETCH_CATEGORY_FILTER] (state, filters) {
    state.filters = filters || []
  }
}
