import PaginationState from '../types/PaginationState';
import { GetterTree } from 'vuex';

export const getters: GetterTree<PaginationState, any> = {
  getProductlist: (state) => state.products,
  getCategory: (state) => state.category,
  getFilterOption: (state) => state.filters
}
