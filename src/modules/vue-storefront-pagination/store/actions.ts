import { ActionTree } from 'vuex';
import PaginationState from '../types/PaginationState';
import { buildFilterProductsQuery } from '@vue-storefront/core/helpers'
import rootStore from '@vue-storefront/core/store'
import { products, entities } from 'config'
import * as types1 from '@vue-storefront/core/modules/catalog-next/store/category/mutation-types'
import * as types from '../store/mutation-types'
import { router } from '@vue-storefront/core/app'
import { changeFilterQuery } from '@vue-storefront/core/modules/catalog-next/helpers/filterHelpers'
import FilterVariant from '@vue-storefront/core/modules/catalog-next/types/FilterVariant'

const actions: ActionTree<PaginationState, any> = {
  async loadCategoryProducts ({ commit, dispatch }, { route, category, pageSize = 50, page = 1, isMobile = false } = {}) {
    const paginationCategory = rootStore.getters['pagination/getCategory'];
    const searchCategory = category || rootStore.getters['category-next/getCategoryFrom'](route.path) || {}

    if (paginationCategory !== searchCategory.id) {
      if (pageSize % 2 !== 0) {
        pageSize = pageSize + 1
      }
      commit(types.CLEAR_CATEGORY_PRODUCTS)
    }

    let paginationProductList = rootStore.getters['pagination/getProductlist'];

    if (paginationProductList[page] && paginationProductList[page].length && Object.keys(route[products.routerFiltersSource]).length) {
      let pageProducts = paginationProductList[page]
      rootStore.commit('category-next/' + types1.CATEGORY_SET_PRODUCTS, pageProducts)
      rootStore.commit('segment/productListView', { category: searchCategory, products: pageProducts })
      return pageProducts
    } else {
      let promotionalImage = (isMobile) ? searchCategory.mobileimage1 : searchCategory.promotionalimage1

      if (promotionalImage && promotionalImage !== null && promotionalImage !== '') {
        if (pageSize % 2 === 0) {
          pageSize = pageSize - 1
        }
      }
      const startPage = (page - 1) * pageSize
      commit(types.FETCH_CURRENT_CATEGORY, searchCategory.id)

      const categoryMappedFilters = rootStore.getters['category-next/getFiltersMap'][searchCategory.id]
      const areFiltersInQuery = !!Object.keys(route[products.routerFiltersSource]).length
      if (!categoryMappedFilters && areFiltersInQuery) { // loading all filters only when some filters are currently chosen and category has no available filters yet
        await rootStore.dispatch('category-next/loadCategoryFilters', searchCategory)
      }
      const searchQuery = rootStore.getters['category-next/getCurrentFiltersFrom'](route[products.routerFiltersSource], categoryMappedFilters)
      commit(types.FETCH_CATEGORY_FILTER, searchQuery)
      let filterQr = buildFilterProductsQuery(searchCategory, searchQuery.filters)
      const { items, perPage, start, total, aggregations, attributeMetadata } = await dispatch('product/findProducts', {
        query: filterQr,
        sort: searchQuery.sort || `${products.defaultSortBy.attribute}:${products.defaultSortBy.order}`,
        includeFields: entities.productList.includeFields,
        excludeFields: entities.productList.excludeFields,
        start: startPage,
        size: pageSize,
        configuration: searchQuery.filters,
        options: {
          populateRequestCacheTags: true,
          prefetchGroupProducts: false,
          setProductErrors: false,
          fallbackToDefaultWhenNoAvailable: true,
          assignProductConfiguration: false,
          separateSelectedVariant: false
        }
      }, { root: true })

      await rootStore.dispatch('category-next/loadAvailableFiltersFrom', {
        aggregations,
        attributeMetadata,
        category: searchCategory,
        filters: searchQuery.filters
      })

      rootStore.commit('category-next/' + types1.CATEGORY_SET_SEARCH_PRODUCTS_STATS, { perPage, start, total })
      rootStore.commit('category-next/' + types1.CATEGORY_SET_PRODUCTS, items)
      commit(types.CLEAR_CATEGORY_PRODUCTS)
      paginationProductList[page] = items
      commit(types.FETCH_CATEGORY_PRODUCTS, paginationProductList)
      rootStore.commit('segment/productListView', { category: searchCategory, products: items })
      return items
    }
  },
  async loadMoreCategoryProducts ({ commit, getters, rootState, dispatch }, payload) {
    let perPage = rootStore.getters['category-next/getCategorySearchProductsStats'].perPage

    if (payload) {
      perPage = payload.perPage
    }

    const { start, total } = rootStore.getters['category-next/getCategorySearchProductsStats']
    const totalValue = typeof total === 'object' ? total.value : total
    if (start >= totalValue || totalValue < perPage) return

    rootStore.commit('category-next/' + types1.CATEGORY_SET_SEARCH_PRODUCTS_STATS, {
      perPage: perPage,
      start: start + perPage,
      total: totalValue
    })

    const searchQuery = getters.getFilterOption
    let filterQr = buildFilterProductsQuery(rootStore.getters['category-next/getCurrentCategory'], searchQuery.filters)
    const searchResult = await dispatch('product/findProducts', {
      query: filterQr,
      sort: searchQuery.sort || `${products.defaultSortBy.attribute}:${products.defaultSortBy.order}`,
      start: start + perPage,
      size: perPage,
      includeFields: entities.productList.includeFields,
      excludeFields: entities.productList.excludeFields,
      configuration: searchQuery.filters,
      options: {
        populateRequestCacheTags: true,
        prefetchGroupProducts: false,
        setProductErrors: false,
        fallbackToDefaultWhenNoAvailable: true,
        assignProductConfiguration: false,
        separateSelectedVariant: false
      }
    }, { root: true })

    return searchResult.items
  },
  async prefetchProducts ({ commit, getters, dispatch }, { page = 1 }) {
    const { total, perPage } = rootStore.getters['category-next/getCategorySearchProductsStats']
    let products = getters.getProductlist
    let pageNumber = Math.ceil(total / perPage)
    let pages = Array.from({ length: pageNumber }, (v, k) => k + 1)
    if ((page <= 5)) {
      pages = pages.slice(0, 10)
    } else if (page >= pageNumber - 5) {
      pages = pages.slice(Math.max(0, pageNumber - 10), pageNumber)
    } else {
      pages = pages.slice(page - 5, (page - 5) + 10)
    }

    products[0] = []
    for (let i = 0; i < pages.length; i++) {
      if (!products[pages[i]] || (products[pages[i]] && products[pages[i]].length === 0)) {
        dispatch('loadMoreCategoryProducts', { perPage: perPage, page: pages[i] }).then(product => {
          products[pages[i]] = product
        })
      }
    }

    commit(types.FETCH_CATEGORY_PRODUCTS, products)
    return true
  },
  async switchSearchFilters ({ dispatch, getters, commit }, filterVariants: FilterVariant[] = []) {
    commit(types.CLEAR_CATEGORY_PRODUCTS)
    let currentQuery = router.currentRoute[products.routerFiltersSource]
    currentQuery.page = 1
    filterVariants.forEach(filterVariant => {
      currentQuery = changeFilterQuery({ currentQuery, filterVariant })
    })
    await rootStore.dispatch('category-next/changeRouterFilterParameters', currentQuery)
  }
};
export default actions;
