import { Module } from 'vuex'
import PaginationState from '../types/PaginationState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const PaginationModuleStore: Module<PaginationState, any> = {
  namespaced: true,
  state: {
    products: [],
    category: 0,
    filters: []
  },
  mutations,
  actions,
  getters
}
