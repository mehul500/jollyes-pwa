import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { PaginationModuleStore } from './store/index';

export const PaginationModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('pagination', PaginationModuleStore)
};
